enum Job {
    Passages = 0x01,
    Pitfalls = 0x02,
    LiteStep = 0x04,
    Dash     = 0x08,
    Learning = 0x10
};
